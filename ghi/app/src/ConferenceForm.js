import React, { useEffect, useState} from 'react'

function ConferenceForm() {

  const [locations, setLocations] = useState([]);
  const [name, setName] = useState('');
  const [starts, setStarts] = useState('');
  const [ends, setEnds] = useState('');
  const [description, setDescription] = useState('');
  const [maximumPresentations, setMaximumPresentations] = useState('');
  const [maximumAttendees, setMaximumAttendees] = useState('');
  const [location, setLocation] = useState('');

  const handleNameChange = event => {
    const value = event.target.value;
    setName(value);
}
  const handleStartsChange = event => {
    const value = event.target.value;
    setStarts(value);
}
  const handleEndsChange = event => {
    const value = event.target.value;
    setEnds(value);
}
  const handleDescriptionChange = event => {
    const value = event.target.value;
    setDescription(value);
}
  const handleMaximumPresenationsChange = event => {
    const value = event.target.value;
    setMaximumPresentations(value);
}
  const handleMaximumAttendeesChange = event => {
    const value = event.target.value;
    setMaximumAttendees(value);
}
  const handleLocationChange = event => {
    const value = event.target.value;
    setLocation(value);
}
  const handleSubmit = async event => {
    event.preventDefault();
    const data = {}

    data.name = name;
    data.starts = starts;
    data.ends = ends;
    data.description = description;
    data.max_presentations = maximumPresentations;
    data.max_attendees = maximumAttendees;
    data.location = location;

    console.log(data);

    const conferenceUrl = 'http://localhost:8000/api/conferences/';
    const fetchConfig = {
        method: "post",
        body: JSON.stringify(data),
        headers: {
        'Content-Type': 'application/json',
        },
    }
      const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
            const newConference = await response.json();
            console.log(newConference);

            setName('');
            setStarts('');
            setEnds('');
            setDescription('');
            setMaximumPresentations('');
            setMaximumAttendees('');
            setLocation('');
        }
  }

  const fetchData = async () => {
    const url = 'http://localhost:8000/api/locations/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      console.log(data)
      setLocations(data.locations);


    }
  }

  useEffect(() => {
    fetchData();
  }, []);

    return (
        <form onSubmit={handleSubmit} id="create-conference-form">
              <div className="form-floating mb-3">
                <input onChange={handleNameChange} value={name} placeholder="Name" required type="text" id="name" name="name" className="form-control"/>
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleStartsChange} value={starts} placeholder="Starts" required type="date" id="starts" name="starts" className="form-control"/>
                <label htmlFor="myDate">Starts</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleEndsChange} value={ends} placeholder="Ends" required type="date" id="ends" name="ends" className="form-control"/>
                <label htmlFor="myDate">Ends</label>
              </div>
              <div className="form-floating mb-3">
                <label htmlFor="FormControlTextarea" className="form-label"></label>
                <textarea onChange={handleDescriptionChange} value={description} required className="form-control" id="description" name="description" type="text"></textarea>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleMaximumPresenationsChange} value={maximumPresentations} placeholder="Maximium presentations" type="number" required id="max_presentations" name="max_presentations" className="form-control"/>
                <label htmlFor="maxPresentations">Maximum presentations</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleMaximumAttendeesChange} value={maximumAttendees} placeholder="Maximium attendees" type="number" required id="max_attendees" name="max_attendees" className="form-control"/>
                <label htmlFor="maxAttendees">Maximum attendees</label>
              </div>
              <div className="mb-3">
                <select onChange={handleLocationChange} value={location} required type="text" id="location" name="location" className="form-select">
                  <option selected value="">Choose a location</option>
                  {
                    locations.map(location => {
                      return (
                        <option key={location.id} value={location.id}>
                          {location.name}
                        </option>
                      )})
                  }
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
    )
}

export default ConferenceForm
