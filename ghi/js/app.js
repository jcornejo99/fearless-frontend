function createCard(name, description, pictureUrl, start, end, location) {


    return `
    <div class="col">
      <div class="shadow-lg p-3 mb-5 bg-body-tertiary rounded my-4 card">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-body-secondary text-muted">${location}</h6>
          <p class="card-text">${description}</p>
        </div>
        <div id="liveAlertPlaceholder"></div>
        <button type="button" class="btn btn-primary" id="liveAlertBtn">Show live alert</button>
        <div class="card-footer">
          ${start} - ${end}
        </div>
      </div>
    </div>
    `;

  }



window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
        const response = await fetch(url);

        if (!response.ok) {
        // console.error('error has occurred');
            throw new Error('Response was not OK. Error has occurred');
        } else {
            const data = await response.json();

        // const conference = data.conferences[0];
        // const nameTag = document.querySelector('.card-title');
        // nameTag.innerHTML = conference.name;
        for (let conference of data.conferences) {
            const detailUrl = `http://localhost:8000${conference.href}`;
            const detailResponse = await fetch(detailUrl)
            if (detailResponse.ok) {
                const details = await detailResponse.json();
                console.log(details);
                const title = details.conference.name;
                // console.log(title);
                const description = details.conference.description;
                const pictureUrl = details.conference.location.picture_url;
                const start = new Date(details.conference.starts).toLocaleDateString()
                const end = new Date(details.conference.ends).toLocaleDateString();
                const location = details.conference.location.name;
                console.log(location);
                // const startDate = details.conference.location

                var alertPlaceholder = document.getElementById('liveAlertPlaceholder')
                var alertTrigger = document.getElementById('liveAlertBtn')

                function alert(message, type) {
                  var wrapper = document.createElement('div')
                  wrapper.innerHTML = '<div class="alert alert-success' + type + ' alert-dismissible" role="alert">' + message + '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button></div>'

                  alertPlaceholder.append(wrapper)
                }

                if (alertTrigger) {
                  alertTrigger.addEventListener('click', function () {
                    alert('Nice, you triggered this alert message!', 'success')
                  })
                }
                const html = createCard(title, description, pictureUrl, start, end, location);
                // console.log(html);

                const column = document.querySelector('#row');
                column.innerHTML += html;

                // const conferenceDetail = details.conference.description;
                // const description = document.querySelector('.card-text');
                // const imageTag = document.querySelector('.card-img-top');
                // imageTag.src = details.conference.location.picture_url;
                // description.innerHTML = conferenceDetail


            }
        }
    }
  } catch (e) {
        console.error(e);
    }
});
